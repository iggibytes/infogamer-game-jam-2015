﻿using UnityEngine;
using System.Collections;

public class HPBar : MonoBehaviour {

    public RectTransform bar;

	public void Set(float percentage)
    {
        bar.sizeDelta = new Vector2(58 * percentage, 4);
    }

    public void Position(Vector3 pos)
    {
        transform.position = Camera.main.WorldToScreenPoint(pos);
    }
}
