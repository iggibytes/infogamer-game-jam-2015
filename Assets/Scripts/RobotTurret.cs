﻿using UnityEngine;
using System.Collections;

public class RobotTurret : MonoBehaviour {

    Enemy target = null;

    float lastShooted;

    Animator animator;

	void Start () 
    {
        animator = GetComponent<Animator>();
	}
	
	void Update () 
    {
        if (!TargetOutOfRange())
        {

            Vector2 fromVector2 = new Vector2(transform.forward.x, transform.forward.z);
            Vector2 toVector2 = new Vector2((transform.position - target.transform.position).x, (transform.position - target.transform.position).z);

            float ang = Vector2.Angle(fromVector2, toVector2);
            Vector3 cross = Vector3.Cross(fromVector2, toVector2);

            if (cross.z > 0)
                ang = 360 - ang;

            transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y + ang + 180, 0);


            if((Time.time - lastShooted) > 0.2f)
            {
                lastShooted = Time.time;
                target.Damage(2.0f);

                animator.SetTrigger("shoot");

                if(target.Dead)
                {
                    target = null;
                }
            }

        }
        else
        {
        }
	}

    void OnDisable()
    {
        target = null;
    }

    void OnEnable()
    {
        StartCoroutine(Search());
    }

    bool TargetOutOfRange()
    {
        if (target != null)
        {
            if ((target.transform.position - transform.position).sqrMagnitude > 50) return true;

            return false;
        }

        return true;
    }

    public GameObject sparkPrefab;
    public Transform leftParticle;
    public Transform rightParticle;

    public void SpawnLeftPaticle()
    {
        GameObject goL = Instantiate(sparkPrefab) as GameObject;
        goL.transform.parent = leftParticle.transform;
        goL.transform.localPosition = Vector3.zero;
        goL.transform.localEulerAngles = Vector3.zero;

        GameObject goR = Instantiate(sparkPrefab) as GameObject;
        goR.transform.parent = rightParticle.transform;
        goR.transform.localPosition = Vector3.zero;
        goR.transform.localEulerAngles = Vector3.zero;

    }

    public void SpawnRightParticle()
    {

    }

    IEnumerator Search()
    {
        while(true)
        {
            yield return new WaitForSeconds(0.1f);

            if (TargetOutOfRange())
            {
                float minDistance = float.MaxValue;

                foreach (Enemy enemy in GameManager.enemies)
                {
                    if (enemy == null) continue;

                    if (!enemy.valid) continue;

                    float distance = (transform.position - enemy.transform.position).sqrMagnitude;
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        target = enemy;
                    }
                }

            }
        }
    }
}
