﻿using UnityEngine;
using System.Collections;

public class HPBarManager : MonoBehaviour {

    public GameObject hpbarPrefab;

    public static HPBarManager main;

    void Start()
    {
        main = this;
    }

    public HPBar New()
    {
        GameObject go = Instantiate(hpbarPrefab);
        go.transform.parent = transform;
        return go.GetComponent<HPBar>();
    }
}
