﻿using UnityEngine;
using System.Collections;

public class RobotCollector : MonoBehaviour {

    public float rotationSpeed = 1;

    [HideInInspector]
    public float modifier = 1.0f;

    float random = 1.0f;

    public LayerMask mineralMask;

    float angles = 0;

    void Start()
    {
        angles = Random.Range(0, 1);
    }

	void Update () 
    {
        Debug.DrawRay(transform.position + new Vector3(0, 10, 0), new Vector3(0, -1, 0));

        if(Physics.Raycast(transform.position + new Vector3(0,10,0), new Vector3(0,-1,0), 100, mineralMask))
        {
            modifier += Time.deltaTime * 4.0f;

            GameManager.main.spaceStuff += 4f * Time.deltaTime;

        } else
        {
            modifier -= Time.deltaTime * 6.0f;
        }

        modifier = Mathf.Clamp(modifier, 0.3f, 6);

        angles = angles + (Time.deltaTime ) * rotationSpeed * modifier;

        transform.localEulerAngles = new Vector3(0, angles - transform.parent.localEulerAngles.y, 0);
	

	}
}
