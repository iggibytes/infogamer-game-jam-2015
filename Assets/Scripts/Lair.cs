﻿using UnityEngine;
using System.Collections;

public class Lair : MonoBehaviour {

    public GameObject spawnPrefab;

    public AnimationCurve curve;

    float spawn;

    public float delay = 0.0f;

    public float targetScale = 0.04619091f;

    float mytime = 0;

	// Use this for initialization
	void Start () {
        transform.localScale = Vector3.zero;

        mytime = 0;
	}
	
	// Update is called once per frame
	void Update () {

        mytime += Time.deltaTime;

        if (mytime < delay) return;

        if(transform.localScale.x < targetScale)
        {
            float scale = Mathf.Clamp(transform.localScale.x + Time.deltaTime / 30.0f, 0, targetScale);
            transform.localScale = new Vector3(scale, scale, scale);
        }

        spawn += curve.Evaluate(mytime - delay) * Time.deltaTime;

        if(spawn > 1)
        {
            spawn -= 1;
            Instantiate(spawnPrefab, transform.position, Quaternion.identity);
        }
	}
}
