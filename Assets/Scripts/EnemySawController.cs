﻿using UnityEngine;
using System.Collections;

public class EnemySawController : MonoBehaviour {

    Transform target = null;

    CharacterController cc;

    public float speed = 3;

    public GameObject spark;

    HPBar bar;

    Enemy enemy;

    float maxHP;

	// Use this for initialization
	void Start () {
        cc = GetComponent<CharacterController>();

        transform.position = new Vector3(transform.position.x, -2.0f, transform.position.z);

        StartCoroutine(Search());
        StartCoroutine(Destroy());

        enemy = GetComponent<Enemy>();
        maxHP = enemy.HP;
    }
	
	// Update is called once per frame
	void Update () 
    {

        if (target == null) return;

        Vector2 fromVector2 = new Vector2(transform.forward.x, transform.forward.z);
        Vector2 toVector2 = new Vector2((transform.position - target.position).x, (transform.position - target.position).z);

        float ang = Vector2.Angle(fromVector2, toVector2);
        Vector3 cross = Vector3.Cross(fromVector2, toVector2);

        if (cross.z > 0)
            ang = 360 - ang;

        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y + ang + 90, 0);


	    if(transform.position.y < 0)
        {
            float newHeight = transform.position.y + Time.deltaTime * 3.0f;
            newHeight = Mathf.Clamp(newHeight, -10.0f, 0);
            transform.position = new Vector3(transform.position.x, newHeight, transform.position.z);
        }
        else
        {
            if (target != null) cc.Move(((transform.position - target.position).normalized * Time.deltaTime * speed) * -1);
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
            GetComponent<Enemy>().valid = true;

            if(bar == null) bar = HPBarManager.main.New();
            bar.Position(transform.position + new Vector3(0, 2, 0));
            bar.Set(enemy.HP / maxHP);
        }

	}

    public LayerMask playerMask;

    void OnDestroy()
    {
        if(bar)
        {
            GameObject.Destroy(bar.gameObject);
        }
    }

    IEnumerator Destroy()
    {
        while(true)
        {
            yield return new WaitForSeconds(0.25f);

            RaycastHit hit;
            Ray ray = new Ray(transform.position + new Vector3(0, 1.2f, 0), transform.right);

            if(Physics.Raycast(ray, out hit, 1.0f, playerMask ))
            {
                target.GetComponent<PlayerController>().Damage(1);

                Instantiate(spark, hit.point, Quaternion.identity);
            }
        }
    }

    IEnumerator Search()
    {
        while(true)
        {
            yield return new WaitForSeconds(Random.Range(0.5f, 2.0f));

            PlayerController[] objects = Object.FindObjectsOfType<PlayerController>();

            float minDistance = float.MaxValue;

            foreach(PlayerController pc in objects)
            {
                float distance = (transform.position - pc.transform.position).sqrMagnitude;
                if (distance < minDistance)
                {
                    minDistance = distance;
                    target = pc.transform;
                }
            }
        }
    }
}
