﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public float HP = 10;
    public bool Dead
    {
        get { return HP <= 0; }
        private set { }
    }

    void Start()
    {
        GameManager.enemies.Add(this);
    }

    public GameObject fleshSpark;


    public void Damage(float damage)
    {
        HP -= damage;

        if(HP <= 0)
        {
            GameManager.enemies.Remove(this);
            Destroy(gameObject);
        }
    }

    public bool valid = false;
}
