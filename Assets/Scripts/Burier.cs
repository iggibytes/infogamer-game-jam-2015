﻿using UnityEngine;
using System.Collections;

public class Burier : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<PlayerController>().ChangeState(PlayerController.State.Collector);
        GetComponent<PlayerController>().Bury();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
