﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
    enum Direction { N = 0, NE = 1, E = 2, SE = 3, S = 4, SW = 5, W = 6, NW = 7 };
    float[] directionAngles = new float[] { 0, 45, 90, 135, 180, 225, 270, 315 };

    public enum State { Normal, Collector, Defensive, Turret, Artillery };

    bool buried = false;

    State state = State.Collector;
    
    public static PlayerController main = null;

    CharacterController characterController;

    [Range(0,1)]
    public float friction;

    public float movingSpeed;
    public float rotationSpeed;

    Vector2 velocity;

    public Transform body;

    Terrain terrain;
    TerrainCollider terrainCollider;

    public LayerMask spawnMask;

    public GameObject rebootPrefab;

    public GameObject modeTurret;
    public GameObject modeCollector;
    public GameObject modeDefensive;
    public GameObject radius;

    public float HP = 10;
    float maxHP;

    HPBar bar;

    

    void Awake()
    {
        state = State.Normal;

        if(!main && GetComponent<Burier>() == null)
        {
            SetAsMain();
        }

        maxHP = HP;
    }


    public void Bury()
    {
        buried = true;
        body.GetComponent<Animator>().SetBool("buried" , true);
    }

    public void SetAsMain()
    {
        main = this;
        Camera.main.GetComponent<CameraControl>().target = this.transform;
    }

    public void ChangeState(State newState)
    {
        if (state == newState) return;

        state = newState;

        modeTurret.SetActive(false);
        modeDefensive.SetActive(false);
        modeCollector.SetActive(false);

        if(state == State.Collector)
        {
            modeCollector.SetActive(true);
        }

        if(state == State.Defensive)
        {
            modeDefensive.SetActive(true);
        }

        if(state == State.Turret)
        {
            modeTurret.SetActive(true);
        }
    }

	void Start () 
    {
        characterController = GetComponent<CharacterController>();
        terrain = Terrain.activeTerrain;
        terrainCollider = terrain.GetComponent<TerrainCollider>();


    }
	
	void Update () 
    {
        
        if (main == this)
        {
            UpdateMain();
        }
        else
        {
            UpdateSecondary();
        }

        if (bar == null)
        {
            if (!GameManager.main.paused)
            {
                bar = HPBarManager.main.New();
            }
        }
        else
        {
            bar.Position(transform.position + new Vector3(0, 1, 0));
            bar.Set(HP / maxHP);
        }
	}


    void FixedUpdate()
    {
        radius.SetActive(false);

        if (main == this)
        {
            FixedUpdateMain();
        }
        else
        {
            FixedUpdateSecondary();
        }
    }

    void UpdateSecondary()
    {

    }


    void UpdateMain()
    {
        Vector3 moveDirection = new Vector3(velocity.x, Physics.gravity.y , velocity.y);

        body.GetComponent<Animator>().SetFloat("walkspeed", velocity.sqrMagnitude);

        float stateModifier = state == State.Normal ? 1.2f : 0.4f;

        characterController.Move(moveDirection * Time.deltaTime * stateModifier);

        body.GetComponent<Animator>().SetFloat("modifier", Mathf.Sqrt(stateModifier));

        float angle = body.transform.localEulerAngles.y % 360;

        if(velocity.sqrMagnitude > 0.0001f)
        {
            body.LookAt(transform.position + moveDirection);
            body.localEulerAngles = new Vector3(0, body.localEulerAngles.y, 0);
        }

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        GameManager.main.HideHelp();

        if(Input.GetMouseButton(1))
        {
            if (terrainCollider.Raycast(ray, out hit, 1000))
            {
                RaycastHit sphereCastHitInfo;

                Debug.DrawLine(hit.point,hit.point + Vector3.forward);

                bool canClone = true;

                canClone = canClone & GameManager.main.spaceStuff > 100;

                if(!canClone)
                {
                    GameManager.main.ShowHelp("YOU NEED 100 MINERALS TO CAST REBOOT");
                }
                
                canClone = canClone & !Physics.Raycast(hit.point + new Vector3(0, 10, 0), new Vector3(0,-1,0), out sphereCastHitInfo, 1000.0f, spawnMask);

                canClone = canClone & ((hit.point - transform.position).sqrMagnitude < 50) & ((hit.point - transform.position).sqrMagnitude > 1);


                radius.SetActive(true);

                if (!canClone)
                {
                    GameManager.main.hologram.SetActive(false);
                    GameManager.main.hologramBad.SetActive(true);
                    GameManager.main.hologramBad.transform.position = new Vector3(hit.point.x, 0.85f + terrain.SampleHeight(hit.point) + 0.5f, hit.point.z);
                }
                else
                {
                    GameManager.main.hologram.SetActive(true);
                    GameManager.main.hologramBad.SetActive(false);
                    GameManager.main.hologram.transform.position = new Vector3(hit.point.x, 0.85f + terrain.SampleHeight(hit.point) + 0.5f, hit.point.z);

                    if(Input.GetMouseButtonDown(0))
                    {
                        GameManager.main.spaceStuff -= 100;
                        StartCoroutine(Reboot(hit.point));
                    }
                }


                //GameManager.main.hologram.transform.position = new Vector3(hit.point.x, 1.85f + terrain.SampleHeight(hit.point) - 1, hit.point.z);
                
            }
        }
        else
        {
            GameManager.main.hologram.SetActive(false);
            GameManager.main.hologramBad.SetActive(false);
        }

        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            ChangeState(State.Normal);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ChangeState(State.Turret);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ChangeState(State.Defensive);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ChangeState(State.Collector);
        }

        float wheel = Input.GetAxisRaw("Mouse ScrollWheel");

        if(wheel != 0 && (Time.time - lastMouseWheen) > 0.1f)
        {
            lastMouseWheen = Time.time;

            if (wheel > 0)
            {
                switch (state)
                {
                    case State.Normal: ChangeState(State.Turret); break;
                    case State.Turret: ChangeState(State.Defensive); break;
                    case State.Defensive: ChangeState(State.Collector); break;
                    case State.Collector: ChangeState(State.Normal); break;
                }
            }

            if (wheel < 0)
            {
                switch (state)
                {
                    case State.Normal: ChangeState(State.Collector); break;
                    case State.Turret: ChangeState(State.Normal); break;
                    case State.Defensive: ChangeState(State.Turret); break;
                    case State.Collector: ChangeState(State.Defensive); break;
                }
            }
        }
    }

    float lastMouseWheen = 0;

    void FixedUpdateMain()
    {
        float horizontalForce = Input.GetAxisRaw("Horizontal");
        float verticalForce = Input.GetAxisRaw("Vertical");

        Vector2 force = (new Vector2(horizontalForce, verticalForce)).normalized;

        velocity = velocity * friction + force * movingSpeed * 0.01f;
    }

    void FixedUpdateSecondary()
    {
    }


    IEnumerator Reboot(Vector3 position)
    {
        main = null;

        GameObject newPlayer = Instantiate(rebootPrefab);
        PlayerController newPlayerController = newPlayer.GetComponent<PlayerController>();
        newPlayer.transform.position = new Vector3(position.x, 0.85f + terrain.SampleHeight(position) + 0.5f, position.z);

        newPlayerController.SetAsMain();
        newPlayerController.GetComponent<CharacterController>().Move(newPlayer.transform.forward * 0.01f);
        this.Bury();

        newPlayerController.ChangeState(state);

        yield return new WaitForSeconds(0.1f);

    }

    public void Damage(float damage)
    {
        if(state == State.Defensive)
        {
            HP -= damage * 0.05f;
        }
        else
        {
            HP -= damage;
        }

        if(HP <= 0)
        {
            if(main == this)
            {
                main = null;
                GameManager.main.GameOver();
            }
            Destroy(gameObject);
        }
    }

    void OnDestroy()
    {
        if(bar)
        {
            GameObject.Destroy(bar.gameObject);
        }
    }
}
