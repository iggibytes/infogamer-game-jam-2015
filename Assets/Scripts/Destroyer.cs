﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {

    public float life = 0.2f;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(life);
        Destroy(gameObject);
    }
}
