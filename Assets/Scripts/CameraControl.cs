﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{
    Vector3 offset;
    Vector3 targetPosition;

    [Range(0,1)]
    public float easing;

    public Transform target;

	void Start () 
    {
        offset = transform.position - target.position;
	}
	
	void Update () 
    {
        if (!target) return;

        Vector3 targetPosition = target.position + offset;

        if(transform)
        {
            transform.position = (Vector3.Lerp(transform.position, targetPosition, easing));
        }
	}
}
