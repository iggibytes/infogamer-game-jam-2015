﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public static GameManager main;

    public GameObject hologram;
    public GameObject hologramBad;

    public float spaceStuff = 0;
    public Text spaceStuffText;

    public GameObject gameOverScreen;
    public GameObject gameStartScreen;
    public GameObject gameWonScreen;
    public GameObject helpText;
    public Text errorText;
    public Text winText;

    public bool paused = true;

    public static HashSet<Enemy> enemies = new HashSet<Enemy>();

    void Awake()
    {
        main = this;
        Time.timeScale = 0;

        gameStartScreen.SetActive(true);

        enemies.Clear();

        HideHelp();

        MeshRenderer[] mrs = hologramBad.GetComponentsInChildren<MeshRenderer>();

        foreach (MeshRenderer mr in mrs)
        {
            mr.sortingOrder = 10;
        }

        mrs = hologram.GetComponentsInChildren<MeshRenderer>();

        foreach (MeshRenderer mr in mrs)
        {
            mr.sortingOrder = 10;
        }
    }

    void Update()
    {
        spaceStuffText.text = "MINERALS: " + ((int)spaceStuff).ToString();

        if(spaceStuff > 1500)
        {
            Win();
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(!paused)
            {
                paused = true;
                Time.timeScale = 0;
                gameStartScreen.SetActive(true);
                helpText.SetActive(false);

            }
        }
    }

    public void GameStart()
    {
        paused = false;
        Time.timeScale = 1;
        gameStartScreen.SetActive(false);

        helpText.SetActive(true);

    }

    public void ShowHelp(string message)
    {
        errorText.text = message;
        errorText.gameObject.SetActive(true);
    }

    public void HideHelp()
    {
        errorText.gameObject.SetActive(false);
    }

    public void GameOver()
    {
        paused = true;
        Time.timeScale = 0;
        gameOverScreen.SetActive(true);
        helpText.SetActive(false);

        Enemy[] objs = Object.FindObjectsOfType<Enemy>();

        PlayerController.main = null;

        GameManager.enemies.Clear();

        foreach (Enemy go in objs)
        {
            Destroy(go.gameObject);
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Win()
    {
        paused = true;
        Time.timeScale = 0;
        gameWonScreen.SetActive(true);
        helpText.SetActive(false);

        int minutes = Mathf.FloorToInt(Time.timeSinceLevelLoad/60);
        int seconds = Mathf.FloorToInt(Time.timeSinceLevelLoad-minutes * 60);

        winText.text = "IT TOOK YOU ONLY " + minutes.ToString() + ":" + seconds.ToString() + " min";

        Enemy[] objs = Object.FindObjectsOfType<Enemy>();

        PlayerController.main = null;

        GameManager.enemies.Clear();

        foreach (Enemy go in objs)
        {
            Destroy(go.gameObject);
        }
    }

    public void TryAgain()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
